# MidiCon X16 ISR

## Genesis

First there was a synth, then another, and so on... So there were synths but not as many hands (neither feet nor noses nor any other appendices). 
Then step sequencers appeared, but they were so painful to use that they just hampered creativity.
So I was hopelessly looking for one tolerable sequencer when I realized I could design my own, which would be just like I want it to be.

## MidCon X16 ISR (waiting for a decent name)

MidCon X16 ISR (waiting for a decent name) is an intuitive 16 steps Midi sequencer, highly accurate (thanks to a ~50µs precision) and pretty musical.
It can switch between two time modes : continuous time and fixed time division per step, in this second mode each step can last from 1/64 to 64 beats. 
This makes it a very rhythmic sequencer, perfectly suitable for basslines. Furthermore it has a joystick for live control over two midi CC parameters!

MidCon X16 ISR (waiting for a decent name) can also record a sequence via midi then quantized it to the tempo, it has memory for saving sequences, a song mode, etc...
And it can use midi or midi-usb!

## Technical

MidCon X16 ISR (waiting for a decent name) is powered by an arduino mega 2560, it has got a 16*2 screen, plenty of buttons, leds, three encoders and a joystick. 
The midi In circuit is isolated by an optocoupler.

    I will post the schematics later

The last (DEV) version is using the great arduino midi library from FortySevenEffects.

I also used the dual mocoLUFA firmware by morecat_lab.
