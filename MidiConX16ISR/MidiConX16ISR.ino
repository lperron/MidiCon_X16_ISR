//need to adjust  gate time
//midi out on pin 1
//midi in on pin 0
#include <LiquidCrystal.h>   //easier for LCD
#include <EEPROM.h>   //for the memory, 2560 has 4KiB of EEPROM ->   2048 int (2B) -> up to 24seq (84 int (notes=80 + timetempo + param2 + last note+1st int(exist?) = 168B) 24 Seq+64B
//#include <MIDI.h>    //we don't need this shit anymore mwahahaha
#include "MidiConX16ISR.h"  //I put some constant variables here



bool testmidiswitch;
HardwareSerial* SerialSelected;
volatile boolean TurnDetected1, TurnDetected2, TurnDetected3;  // need volatile for Interrupts
volatile boolean rotationdirection1, rotationdirection2, rotationdirection3;  // CW or CCW rotation
const int pinpush[] = {
  22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37
};  //note selection buttons
const int redled[] = {
  38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53
};  //note led pins
const int midiswitch=A14; //
const int blueled = 13;  //tempo led pin
const int pushmem = 16;  //memory push button pin
const int rgbbpin = 14;  //rgb led pin blue
const int rgbgpin = 15;  //rgb led pin green
const int X_pin = A2; // analog pin connected to X output joystick
const int Y_pin = A1; // analog pin connected to Y output joystick
int x = 100;      //value of x axis (joystick)
int xprev = 100;    //to compare with x to see if we touch the joystick, if not, no midi CC message sended
int y = 100;  //value of y axis (joystick)
int yprev = 100;  //idem Willem
/*unsigned*/int note[16][5];  //note Vel  timeDiv time timedivms  !!!!!!!!!store this!!!!!!!!!
int heartpin = 0;   // heartbeat sensor pin
//int RotaryPosition1 = 0;  //position encoder 1
const int PinCLK1 = 2; // Generating interrupts using CLK signal encoder 1
const int PinDT1 = 5;  // Reading DT signal encoder 1
const int PinSW1 = 6;  // Reading Push Button switch encoder 1
const int PinCLK2 = 21; // Generating interrupts using CLK signal encoder 2
const int PinDT2 = 3;  // Reading DT signal encoder 2
const int PinSW2 = 4;  // Reading Push Button switch encoder2
const int PinCLK3 = 20;
const int PinDT3 = A5;
const int PinSW3 = A6;
const int pushsetting = 17;
unsigned long int uptimepushsetting = 0;
unsigned long int uptimered[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};  //uptime red leds
unsigned int noteselect = 0;   //explicit punk !
const double alpha = 0.75;  //constant calibration heartbeat sensor
const int period = 100;   //same
double change = 0.0;  //same
double minval = 0.0;  //same
double tempo = 80;  //tempo, bpm
unsigned int timetempo = 0;  //tempo millis      !!!!!!!store this!!!!!!!
unsigned long int uptimeblue;
int param = 0; //0 = note, 1=vel,  2 =time  !!!!!!!store this!!!!!
int param1 = 0;  //param of the 1st encoder  !!!!!!!!store this!!!!!!!!
unsigned int param2 = 2;  //param of the second encoder  !!!!!!store this!!!!!!!
int noteplayed = 0;  //explicit
unsigned long int noteplayedtime = 0;  //played time of the note
unsigned long int uptimeencoder1 = 0, uptimeencoder2 = 0, uptimeencoder3 = 0; //uptime 1st encoder
unsigned long int uptimepush = 0;  //note select push uptime
unsigned long int uptimepushmem = 0;//idem with the mem push
bool ispush = false;  //for mempush
bool anotherispush[16];      //for the note pushes
bool isplaypush = false; //for play/pause button
bool ok = false;    //this one and the next one and the next next one are used in some awfull do while loops when waiting for the fuckin' user to interact with the seq
bool exitmem = false;
bool answer = false;
unsigned int lastnote = 15;    //store this!!!!   length of the sequence selected by the user
bool heartok = false;
LiquidCrystal lcd(7, 8, 9, 10, 11, 12); //pin LCD
int address;  //ADDRESS in the EEPROM for memory function
unsigned long int joytime = 0; // millis when we sent the last midi CC channel
unsigned long int timedisplaytempo = 666666666666;
int tempomult = 1;
bool lcdchange = false;
//setting parameters
int midichannel = 1; //explicit
int CCX = 102; //CC parameters joystick x axis
int CCY = 103;
bool exitsetting = true;
int settingselected = 0;
//play/pause/stop parameters
bool play = true;
int pinplay = A3;
int pinstop = A4;
unsigned long int uptimeplay;
unsigned long int uptimestop;
//Clock stuff
int clock = 0; //=>clock mode => 0=internal, 1=midiin, 2:master.. still in progress, now only internal works, some bugs with midi in and master not implemented. And when i said bugs it's something like a non-reset-solved bug, so be carefull.
//well midi in work now, could be more precise but it's ok //now very accurate.
int play_flag = 0;
byte data;
unsigned long int timeclock = 10000;
unsigned long int previoustimeclock = 10000;
unsigned long int averageclock = 10000;
unsigned long int previousaverageclock = 10000;
unsigned long int timetab[5];
int itab = 0;
bool calc = false;
int previoustempo = 80;
double consttimer5 = 16000000. / 24 / 64 * 60;
//keyboard tracking suff
bool keyboard = false;
byte midiInChannel = 1;
int noteoffset = 48;
int previousnoteoffset = 48;
volatile byte notebyte = 0;
volatile byte velocitybyte = 0;
volatile bool flagnote = false;
volatile int nbbyte = 0;
byte bnote[3];
int keyreference = 48; //C4
int previouskeyreference = 48;
//record stuff
bool record = false;
int keynote[16][3];
unsigned long int temptime[16];
int state[] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
int noterecorded = 0;
int noteended = 0;
const int pushrecord = A7;
unsigned long int uptimepushrecord;
//song mode
bool song = false;
int seqplayed = 0;
int seqseq[1024];
//order stuff
int playmode=0;//normal, reverse, updown, random, random no repeat
bool updown=0; //0=normal mode, 1=reverse mode,
int randstep=0;
bool randok[]={
  1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
bool mem=false;


ISR(TIMER3_COMPA_vect) {    //midi out ISR
  //note playing isr attached to timer 3, compare mode  // for midi notes and CC
  if (play == true)
  {
    if (param2 == 2 and (millis() - noteplayedtime > (note[noteplayed][4])))
    {
      PlayNote2();  //play note with param 2
    }
    else if (param2 == 3 and (millis() - noteplayedtime > note[noteplayed][3]))
    {
      PlayNote3();  //play note with param 3
    }
  }
  if ((millis() - joytime) > 10)
  {
    joytime = millis();
    JoyD();
  }
}

ISR(TIMER4_COMPA_vect) {     //midi in ISR
  //Clock inREALLY accurate, need to adjust some constant like the size of the tab and maybe OCR4A and it could be even better, maybe look at stop function (bug) (nope)
  if (clock == 1 or keyboard == true or record == true )
  {
    int serial = SerialSelected->available();
    if (serial > 0) 
    {
      data = SerialSelected->read();
      if (flagnote == 0) 
      {
        if (data == 0xfa and clock == 1) 
        { //midi start
          play_flag = 1;
          play = true;
        }
        else if (data == 0xfb and clock == 1) 
        { //continue
          play_flag = 1;
          play = true;
        }
        else if (data == 0xfc and clock == 1) 
        { //stop
          play_flag = 0;
        }
        else if ((data == 0xf8) and (play_flag == 1) and clock == 1) 
        { //midi clock +play
          timeclock = micros();
          timetab[itab] = timeclock - previoustimeclock;
          previoustimeclock = timeclock;
          itab++;
          if (itab == 5)
          {
            itab = 0;
            Sync();
          }
        }
        else if ((keyboard == true or record == true) and (data == (0x90 + (byte)(midiInChannel - 1)) or data == (0x80 + (byte)(midiInChannel - 1))))
        {
          flagnote = true;
          nbbyte = 0;
          bnote[0] = data;
          nbbyte++;
        }
      }
      else if (flagnote == true) {
        bnote[nbbyte] = data;
        nbbyte++;
        if (nbbyte == 3)
        {
          nbbyte = 0;
          flagnote = false;
          notebyte = bnote[1];
          velocitybyte = bnote[2];
          if (bnote[0] == (0x80 + (byte)(midiInChannel - 1)))
            velocitybyte = 0;
          if (velocitybyte > 0 and record == false and keyboard == true)
            keyboardTracking();
          else if (velocitybyte > 0 and record == true)
            keyboardrecordnoteon();
          else if (velocitybyte == 0 and record == true)
            keyboardrecordnoteoff();
        }
      }
    }
  }
}

ISR(TIMER5_COMPA_vect) {
  if (clock == 2 and play == true)
  {
    SerialSelected->write(0xf8);
  }
}

void isr1 ()  {      //interrupt for the first encoder
  //maybe we need to debounce this !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if (digitalRead(PinCLK1))    //U just have to read something like "Rotary Encoders for Dummy", no offense
  {
    rotationdirection1 = !digitalRead(PinDT1);
  }
  else
  {
    rotationdirection1 = digitalRead(PinDT1);
  }
  TurnDetected1 = true;
}

void isr2 ()  {
  if (digitalRead(PinCLK2))
  {
    rotationdirection2 = digitalRead(PinDT2);
  }
  else
  {
    rotationdirection2 = !digitalRead(PinDT2);
  }
  TurnDetected2 = true;
}

void isr3 ()  {
  if (digitalRead(PinCLK3))
  {
    rotationdirection3 = digitalRead(PinDT3);
  }
  else
  {
    rotationdirection3 = !digitalRead(PinDT3);
  }
  TurnDetected3 = true;
}

void saveSeq(int seqNumber)  //we store all on neighbour bytes, JA, ORGANIZATION BIATCH !!!
//exist?  timetempo  param2  note
{
  int address = 168 * seqNumber;
  saveInt(address , 6666);  //reading the first address of a slot we can know if the slot is empty or not
  saveInt(address + 2, timetempo);
  saveInt(address + 4, param2);
  saveInt(address + 6, lastnote);
  address += 8;
  for (int i = 0; i < 5; i++)
  {
    for (int j = 0; j < 16; j++)
    {
      if (keyboard == true)
      {
        if (i == 0)
          saveInt(address, note[j][i] - noteoffset + keyreference);
        else
          saveInt(address, note[j][i]);
      }
      else
        saveInt(address, note[j][i]);
      address += 2;
    }
  }
}

void saveSettings()
{
  int address = 4032;
  saveInt(address , 6666);
  saveInt(address + 2, midichannel);
  saveInt(address + 4, CCX);
  saveInt(address + 6, CCY);
  saveInt(address + 8, clock);
  saveInt(address + 10, midiInChannel);
  saveInt(address + 12, keyboard);
  saveInt(address + 14, keyreference);
  saveInt(address + 16, playmode);
}

void loadSettings()
{
  int address = 4032;
  unsigned int exist = readInt(address);
  if (exist == 6666)
  {
    midichannel = readInt(address + 2);
    CCX = readInt(address + 4);
    CCY = readInt(address + 6);
    clock = readInt(address + 8);
    midiInChannel = readInt(address + 10);
    keyboard = readInt(address + 12);
    keyreference = readInt(address + 14);
    playmode = readInt(address + 16);
  }
  else
  {
    midichannel = 1;
    CCX = 102;
    CCY = 103;
    clock = 0;
    midiInChannel = 1;
    keyboard = false;
    keyreference = 48;
    playmode=0;
    updown=0;
  }
  if (clock == 2 and play == true)
    SerialSelected->write(0xfa);
}

void loadSeq(int seqNumber)
{
  int address = 168 * seqNumber;
  unsigned int exist = readInt(address);
  if (exist == 6666)  //there is a sequence in this slot
  {
    address += 2;
    if (clock != 1)
      timetempo = readInt(address);
    address += 2;
    param2 = readInt(address);
    address += 2;
    lastnote = readInt(address);
    address += 2;
    for (int i = 0; i < 5; i++)
    {
      for (int j = 0; j < 16; j++)
      {
        note[j][i] = readInt(address);
        address += 2;
      }
    }
    if (clock != 1)
    {
      tempo = (int)(((1.) / (timetempo)) * 60000);
      OCR5A = consttimer5 / tempo;
    }
    else
      Sync();
  }
  else  //no seqence, we load a basic one
  {
    tempo = 80;
    timetempo = (double)((1) / (tempo)) * 60000;
    OCR5A = consttimer5 / tempo;
    for (int i = 0; i < 16; i++)
    {
      note[i][0] = 48; //note C4
      note[i][1] = 45; //vel
      note[i][2] = 6;  //time div 1
      note[i][3] = 500; //continuous note duration, 1s
      note[i][4] = 1 * timetempo;
    }
    param2 = 2;
    lastnote = 15;
  }
  noteoffset = keyreference;
  previousnoteoffset = keyreference;
}

void saveInt(int address, unsigned int val)  //same book as for isr1 but with EEPROM instead of rotary encoders, no offense
{
  //we need to store this int in two different Bytes so we cut itself in two mouahaha
  unsigned char right = val & 0x00FF; //8 right bits
  unsigned char left = (val >> 8) & 0x00FF;  //we shift and put the 8 left bits
  EEPROM.write(address, left) ;
  EEPROM.write(address + 1, right) ;
}

unsigned int readInt(int address)   // So trivial, just reverse the previous function... just think like a computer, with bits, 0 & 1
{
  unsigned int val = 0 ;
  unsigned char left = EEPROM.read(address); //XXXXXXXX
  unsigned char right = EEPROM.read(address + 1); //YYYYYYYY
  val = left ;    // XXXXXXXX
  val = val << 8 ;  // XXXXXXXX+00000000
  val = val | right ; //XXXXXXXX+YYYYYYYY
  return val ;  // XXXXXXXXYYYYYYYY
}
//this midi personnal implementation is still in progress, doesn't work quite well for the moment
//problem when turning note knob, sometimes the previous note is not released
//resulting a shitty holded note
// but I hope resolving that, I don't like libraries...
//Well... Now it works !!!

void MidiNoteOn(byte channel, int note, byte velocity)    //please refer to midi standard reference
{
  if (note > 127)
    note = 127;
  if (note < 0) //NOT USEFULL SINCE BYTE ALWAYS>0  NEED TO PASS INT
    note = 0;
  byte midiMessage = 0x90 + (channel - 1);
  SerialSelected->write(midiMessage);
  SerialSelected->write(note);
  SerialSelected->write(velocity);
}

void MidiNoteOff(byte channel, int note)
{
  if (note > 127)
    note = 127;
  if (note < 0)
    note = 0;
  byte midiMessage = 0x80 + (channel - 1);
  byte velocity = 0;
  SerialSelected->write(midiMessage);
  SerialSelected->write(note);
  SerialSelected->write(velocity);
}

void MidiCC(byte channel, byte CCparam, byte value)
{
  byte midiMessage = 0xB0  + (channel - 1);
  SerialSelected->write(midiMessage);
  SerialSelected->write(CCparam);
  SerialSelected->write(value);
}

void Save()    //all this function is only used to interact with the user and save a seq
{
  digitalWrite(redled[noteplayed], LOW);
  digitalWrite(rgbgpin, HIGH);  //to signal we have enterring the save mode
  ok = false;  // boolean condition to exit the first do while
  do {    //true
    for (int i = 0; i < 16; i++)
    {
      if (readInt(168 * i) == 6666)
      {
        digitalWrite(redled[i], HIGH);
      }
    }
    if ((digitalRead(pushmem) == LOW) and (millis() - uptimepushmem > 1000))
    {
      exitmem = true;
      uptimepushmem = millis();
    }
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Save a Sequence");
    lcd.setCursor(0, 1);
    lcd.print("Select a Slot");
    for (int i = 0; i < 16; i++)
    {
      if (readInt(168 * i) == 6666)
      {
        digitalWrite(redled[i], HIGH);
      }
    }
    do {   //true
      for (int i = 0; i < 16; i++)
      {
        if (readInt(168 * i) == 6666)
        {
          digitalWrite(redled[i], HIGH);
        }
      }
      address = -1;
      for (int i = 0; i < 16; i++) //check each button to see if the user select a slot
      {
        if ((digitalRead(pushmem) == LOW) and (millis() - uptimepushmem > 500))  //to abort the process
        {
          exitmem = true;
          uptimepushmem = millis();
        }
        if ((digitalRead(pinpush[i]) == LOW) and (millis() - uptimepush > 250))   //true
        {
          lcd.clear();
          address = 168 * i;
          if (readInt(address) == 6666)   //if 6666 is the first octet of a saved sequence
            lcd.print("Slot not Empty");
          else
            lcd.print("Slot Empty");
          lcd.setCursor(0, 1);
          uptimepush = millis();
          lcd.print("Are You Sure?");
          digitalWrite(redled[i], HIGH);  //to incate which slot is selected
          answer = false;
          do {
            for (int i = 0; i < 16; i++)
            {
              if (readInt(168 * i) == 6666)
              {
                digitalWrite(redled[i], HIGH);
              }
            }
            if ((digitalRead(pushmem) == LOW) and (millis() - uptimepushmem > 250))  //to abort the process
            {
              exitmem = true;
              uptimepushmem = millis();
            }
            answer == false;
            if ((digitalRead(pinpush[0]) == LOW) and (millis() - uptimepush > 250))  //YES I WANT
            {
              ok = true;
              answer = true;
              lightshow();
              saveSeq(i);
              uptimepush = millis();            
            }
            if ((digitalRead(pinpush[1]) == LOW) and (millis() - uptimepush > 250))  //nope
            {
              ok = false;
              answer = true;
              uptimepush = millis();
            }
          }
          while (answer == false and exitmem == false);
          digitalWrite(redled[i], LOW);
        }
      }
    }
    while (address < 0 and exitmem == false);
  }
  while (ok == false and exitmem == false);  //false
  lcd.setCursor(0, 0);
  lcd.print("                 ");
  lcd.setCursor(0, 1);
  lcd.print("                   ");
  digitalWrite(rgbgpin, LOW);
  for (int i = 0; i < 16; i++)
    digitalWrite(redled[i], LOW);
}

void Load()  //just like the previous function but to load a seq //i m not going to comment this
{
  digitalWrite(rgbbpin, HIGH);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Load a Sequence");
  lcd.setCursor(0, 1);
  lcd.print("Select a Slot");
  do {
    address = -1;
    for (int i = 0; i < 16; i++)
    {
      if (readInt(168 * i) == 6666)
      {
        digitalWrite(redled[i], HIGH);
      }
    }
    for (int i = 0; i < 16; i++) //make the 5 it
    {
      if ((digitalRead(pushmem) == LOW) and (millis() - uptimepushmem > 250))
      {
        exitmem = true;
        uptimepushmem = millis();
      }
      if ((digitalRead(pinpush[i]) == LOW) and (millis() - uptimepush > 250))   //true
      {
        lcd.clear();
        uptimepush = millis();
        address = 168 * i;
        if (readInt(address) == 6666)   //if 6666is the first octet of a saved sequence
        {
          MidiNoteOff(midichannel, note[noteplayed][0]); //note off
          lightshow();
          loadSeq(i);
        }
        else
        {
          lcd.print("Slot Empty");
          delay(1000);
          lightshow();
          address = -1;
          lcd.clear();
          lcd.setCursor(0, 0);
          lcd.print("Load a Sequence");
          lcd.setCursor(0, 1);
          lcd.print("Select a Slot");
        }
        
      }
    }
  }
  while (address < 0 and exitmem == false);
  digitalWrite(rgbbpin, LOW);
  for (int i = 0; i < 16; i++)
    digitalWrite(redled[i], LOW);
}

void PlayNote2()    //playing a note using time div
{
  MidiNoteOff(midichannel, note[noteplayed][0]); //we stop th previous one
  if(mem==false)
    digitalWrite(redled[noteplayed], LOW);  //we turn off the previous led
  nextstep();

  // if (noteplayed <= lastnote)
  {
    if(mem==false)
      digitalWrite(redled[noteplayed], HIGH);  //we turp up the appropriate led
    MidiNoteOn(midichannel, note[noteplayed][0], note[noteplayed][1]); //and we play a note
    noteplayedtime = millis();
  }
}

void PlayNote3()      //play a note with continuous time  //same functionnement as the last function
{
  MidiNoteOff(midichannel, note[noteplayed][0]);
  if(mem==false)
    digitalWrite(redled[noteplayed], LOW);
  nextstep();
  {
    if(mem==false)
      digitalWrite(redled[noteplayed], HIGH);
    MidiNoteOn(midichannel, note[noteplayed][0], note[noteplayed][1]);
    noteplayedtime = millis();
  }
}
void nextstep()
{
  if(playmode==0)
  {
    if (noteplayed < lastnote )
      noteplayed++;
    else
    {
      if (song == true)
      {
        seqplayed++;
        if (seqseq[seqplayed] == -1 or seqplayed == 1028)
          seqplayed = 0;
        loadSeq(seqseq[seqplayed]);
      }
      noteplayed = 0;
    }
  }
  else if(playmode==1)
  {
    if(noteplayed>lastnote)
      noteplayed=lastnote;
    else if(noteplayed>0)
      noteplayed--;
    else
    {
      if(song==true)
      {
        seqplayed++;
        if (seqseq[seqplayed] == -1 or seqplayed == 1028)
          seqplayed = 0;
        loadSeq(seqseq[seqplayed]);
      }
      noteplayed=lastnote;
    }
  }
  else if(playmode==2)
  {
    if(updown==0)
    {
      if(noteplayed>lastnote)
        noteplayed=0;

      if (noteplayed < lastnote )
        noteplayed++;
      else
      {
        noteplayed--;
        updown=1;
      }
    }
    else if(updown==1)
    {
      if(noteplayed>lastnote)
        noteplayed=lastnote;
      else if(noteplayed>0)
        noteplayed--;
      else
      {
        if(song==true)
        {
          seqplayed++;
          if (seqseq[seqplayed] == -1 or seqplayed == 1028)
            seqplayed = 0;
          loadSeq(seqseq[seqplayed]);
        }
        noteplayed++;
        updown=0;
      }
    }
  } 
  else if(playmode==3)
  {
    if(randstep<lastnote)
    {
      randstep++;
      noteplayed=random(0,lastnote+1);
    }
    else
    {
      randstep=0;
      if(song==true)
      {
        seqplayed++;
        if (seqseq[seqplayed] == -1 or seqplayed == 1028)
          seqplayed = 0;
        loadSeq(seqseq[seqplayed]);
      }
    } 
  }
  else if(playmode==4)
  {
    if(randstep<lastnote)
    {
      int temp=random(0,lastnote+1-randstep);
      int noteok=0;
      for(int i=0;i<=lastnote;i++)
      {
        if(randok[i]==true) 
        {    
          if(noteok==temp)
          {
            noteplayed=i;
            randok[i]=false; 
            break;       
          }
          noteok++; 
        }   
      }
      randstep++;
    }
    if(randstep>=lastnote)
    {
      randstep=0;
      for(int i=0;i<16;i++)
      {
        randok[i]=true;
      }
      if(song==true)
      {
        seqplayed++;
        if (seqseq[seqplayed] == -1 or seqplayed == 1028)
          seqplayed = 0;
        loadSeq(seqseq[seqplayed]);
      }
    } 
  }
}
void JoyD()  //joystick
{
  //we read values of the joystick
  x = ((double)analogRead(X_pin) / 1017) * 127; //param102  cut
  y = ((double)analogRead(Y_pin) / 1017) * 127; //param 103 res
  if (x != xprev)  //we send midi CC only if the value changes
  {
    MidiCC(midichannel, CCX, (int)x);
    xprev = x;
  }
  if (y != yprev)  //same
  {
    MidiCC(midichannel, CCY, (int)y);
    yprev = y;
  }
}

void HardestButtonToButton(int k)  //we read the five note push and do what we have to do
{
  if (digitalRead(pinpush[k]) == LOW and (millis() - uptimepush > 250) and anotherispush[k] == false)  //push + debouncing, select a note
  {
    lcd.setCursor(0, 1);
    lcd.clear();
    noteselect = k;
    uptimepush = millis();
    anotherispush[k] = true;
  }
  else if (digitalRead(pinpush[k]) == LOW and (millis() - uptimepush > 1000) and anotherispush[k] == true)  //long push, change the length of the seq
  {
    lastnote = k;
  }
  else if (digitalRead(pinpush[k]) == HIGH)  //well.. the user has released the push
    anotherispush[k] = false;
}

void TurnEncoder1()  //when the encoder is turned
{
  param = param1;
  lcd.setCursor(0, 1);
  lcd.clear();
  if (param == 0) //we change the pitch
  {
    if (noteplayed == noteselect) //to avoid midi bug like note not released
      MidiNoteOff(midichannel, note[noteselect][param1]);  //maybe send a note on after this !!!!!!!!!!!TO DO!!!!!!!!!!!!!
    //MIDI.sendNoteOff(note[noteselect][param1],0,midichannel);
    if (rotationdirection1 and note[noteselect][param] < 127) 
    {
      note[noteselect][param1] += 1;
    } // decrase Position by 1
    if (rotationdirection1 == false and note[noteselect][param] > 0 ) 
    {
      note[noteselect][param1] -= 1;
    } // increase Position by 1
  }
  else if (param == 1) //velocity
  {
    if (rotationdirection1 and note[noteselect][param] < 127) 
    {
      note[noteselect][param1] += 1;
    } // decrase Position by 1
    if (rotationdirection1 == false and note[noteselect][param] > 0 ) 
    {
      note[noteselect][param1] -= 1;
    }
  }
  TurnDetected1 = false;
}

void PushEncoder1() //the user has push the encoder so we change the mode pitch<->velocity
{
  if (param1 == 0)
    param1 = 1;
  else
    param1 = 0;
  param = param1;
  lcd.setCursor(0, 1);
  lcd.clear();
  uptimeencoder1 = millis();
}

void TurnEncoder2()
{
  lcd.setCursor(0, 1);
  lcd.clear();
  param = param2;
  if (param2 == 2)  //time div
  {
    if (rotationdirection2 and note[noteselect][param] > 0) 
    {
      note[noteselect][param] -= 1;
    } // decrase Position by 1
    if (rotationdirection2 == false and note[noteselect][param] < 24 ) 
    {
      note[noteselect][param] += 1;
    } // increase Position by 1       //it works
    note[noteselect][4] = timetempo * timedivms[(int)note[noteselect][param]];
    if (note[noteselect][4] == 0)
      note[noteselect][4] = 1;
  }
  if (param2 == 3)  //really need to find a good scale!!!!  continuous time
  {
    if (rotationdirection2 and note[noteselect][param] > 50) {
      note[noteselect][param] -= 50;
    } // decrase Position by 1
    if (rotationdirection2 == false and note[noteselect][param] < 40000 ) {
      note[noteselect][param] += 50;
    }
  }
  TurnDetected2 = false;
}

void PushEncoder2()    //swith timediv<->continuous
{
  if (param2 == 2)
    param2 = 3;
  else
    param2 = 2;
  param = param2;
  lcd.setCursor(0, 1);
  lcd.clear();
  uptimeencoder2 = millis();
}

void BlueLedBlink()//led tempo
{
  if ((millis() - uptimeblue) >= timetempo)
  {
    digitalWrite(blueled, HIGH);
    uptimeblue = millis();
  }
  if ((millis() - uptimeblue) >= (timetempo / 2.) and (millis() - uptimeblue ) < timetempo)
    digitalWrite(blueled, LOW);
}

void setTempo()
{
  if (clock == 0 or clock == 2) //internal or master
  {
    if (TurnDetected3)  
    {
      lcdchange = true;
      if (tempomult == 1) 
      {
        if (rotationdirection3 and tempo > 1) 
        {
          tempo -= 1;
        } // decrase Position by 1
        if (rotationdirection3 == false and tempo < 666 ) 
        {
          tempo += 1;
        } // increase Position by 1
      }
      if (tempomult == 10) {
        if (rotationdirection3 and tempo > 11) 
        {
          tempo -= 10;
        } // decrase Position by 1
        if (rotationdirection3 == false and tempo < 656 ) 
        {
          tempo += 10;
        } // increase Position by 1
      }
      TurnDetected3 = false;
      timetempo = (unsigned int)((1.) / (tempo) * 60000);
      OCR5A = consttimer5 / tempo;

      for (int i = 0; i < 16; i++)
      {
        note[i][4] = timetempo * timedivms[(int)note[i][2]];
      }
      timedisplaytempo = millis();
      lcd.clear();
      lcd.print("Tempo");
      lcd.setCursor(0, 1);
      lcd.print(tempo);
    }
    if (!(digitalRead(PinSW3)) and millis() - uptimeencoder3 >= 500)
    {
      lcdchange = true;
      uptimeencoder3 = millis();
      if (tempomult == 1)
        tempomult = 10;
      else if (tempomult == 10)
        tempomult = 1;
      timedisplaytempo = millis();
      lcd.clear();
      lcd.print("Tempo");
      lcd.setCursor(0, 1);
      lcd.print(tempo);
    }
    TurnDetected1 = false;
    TurnDetected2 = false;
    TurnDetected3 = false;
  }
  else if (clock == 1)
  {
    if (TurnDetected3)  
    {
      lcdchange = true;
      // increase Position by 1
      TurnDetected3 = false;
      timedisplaytempo = millis();
      lcd.clear();
      lcd.print("Tempo");
      lcd.setCursor(0, 1);
      lcd.print(tempo);
    }
    if (!(digitalRead(PinSW3)) and millis() - uptimeencoder3 >= 500)
    {
      lcdchange = true;
      uptimeencoder3 = millis();
      timedisplaytempo = millis();
      lcd.clear();
      lcd.print("Tempo");
      lcd.setCursor(0, 1);
      lcd.print(tempo);
    }
  }
}

void Memory_All_alone_in_the_moonlight_I_can_smile_at_the_old_days_I_was_beautiful_then_I_remember_The_time_I_knew_what_happiness_was_Let_the_memory_live_again()
{
  //the three next main conditions are used to access memory functions, short press -> load, long push -> save
  //so we look if the push button is push or not
  //if it is, relook it 0.5s after to see if it's a long or a short push.
  if (digitalRead(pushmem) == HIGH)
    ispush == false;
  if (digitalRead(pushmem) == LOW and ispush == false and (millis() - uptimepushmem > 250)) //starting time pushmem
  {
    uptimepushmem = millis();
    ispush = true;
  }
  if ((millis() - uptimepushmem > 500) and ispush == true)  //    Memory functions
  {
    mem=true;
    // MidiNoteOff(midichannel,note[noteplayed][0]);  //note off
    //MIDI.sendNoteOff(note[noteplayed][0],0,midichannel);    //it's just better without notes
    digitalWrite(redled[noteplayed], LOW);
    uptimepushmem = millis();
    if (digitalRead(pushmem) == LOW )
    {
      song = false;
      Save();    //function used for saving seq
    }
    else if (digitalRead(pushmem) == HIGH )
    {
      song = false;
      Load();  //function used for loading seq
    }
    ispush = false; //user must have release this f*ckin' button, else he is an asshole so we don't even care about it!
    exitmem = false;
    TurnDetected1 = false;
    TurnDetected2 = false;
    TurnDetected3 = false;
    uptimepushmem = millis();
    mem=false;
    lcd.clear();
  }
}

void printSettings()
{
  switch (settingselected)
  {
  case 0 :
    {
      lcd.print("Play Mode");
      lcd.setCursor(0,1);
      if(playmode==0)
        lcd.print("Normal");
      if(playmode==1)
        lcd.print("Reverse");
      if(playmode==2)
        lcd.print("Up & Down");
      if(playmode==3)
        lcd.print("Random");
      if(playmode==4)
        lcd.print("Random No Repeat");
      break;
    }
  case 1 :
    {
      lcd.print("CC Joystick X");
      lcd.setCursor(0, 1);
      lcd.print(CCX);
      break;
    }
  case 2 :
    {
      lcd.print("CC Joystick Y");
      lcd.setCursor(0, 1);
      lcd.print(CCY);
      break;
    }
  case 3 :
    {
      lcd.print("Midi Out Channel");
      lcd.setCursor(0, 1);
      lcd.print(midichannel);
      break;
    }
  case 4 :
    {
      lcd.print("Midi In Channel");
      lcd.setCursor(0, 1);
      lcd.print(midiInChannel);
      break;
    }
  case 5 :
    {
      lcd.print("KeyB Tracking");
      lcd.setCursor(0, 1);
      if (keyboard == true)
        lcd.print("Yes");
      else
        lcd.print("No");
      break;
    }
  case 6 :
    {
      lcd.print("key Reference");
      lcd.setCursor(0, 1);
      lcd.print(humannotes[keyreference]);
      break;
    }
  case 7 :
    {
      lcd.print("Clock");
      lcd.setCursor(0, 1);
      if (clock == 0)
        lcd.print("Internal");
      else if (clock == 1)
        lcd.print("Midi In");
      else if (clock == 2)
        lcd.print("master");
      break;
    }
  case 8 :
    {
      lcd.print("Time Quant");
      lcd.setCursor(0, 1);
      lcd.print("Hit Yes Button");
      break;
    }
  case 9 :
    {
      lcd.print("Load Blank Seq");
      lcd.setCursor(0, 1);
      lcd.print("Hit Yes Button");
      break;
    }
  case 10 :
    {
      lcd.print("Save Settings");
      lcd.setCursor(0, 1);
      lcd.print("Hit Yes Button");
      break;
    }
  case 11 :
    {
      lcd.print("MidiCon-X16-ISR");
      lcd.setCursor(0, 1);
      lcd.print("Ver 3.0.02 CK");
      break;
    }
  }
}

void Settings ()
{
  if ((digitalRead(pushsetting) == LOW) and ((millis() - uptimepushsetting) > 250) )
  {
    exitsetting = false;
    uptimepushsetting = millis();
    lcd.clear();
    printSettings();
    do
    {
      if (digitalRead(pushsetting) == LOW and ((millis() - uptimepushsetting) > 250) )
      {
        uptimepushsetting = millis();
        exitsetting = true;
      }
      if (TurnDetected1)  
      {
        lcd.clear();
        if (rotationdirection1 and settingselected < 11) 
        {
          settingselected += 1;
        } // decrase Position by 1
        if (rotationdirection1 == false and settingselected > 0 ) 
        {
          settingselected -= 1;
        } // increase Position by 1
        TurnDetected1 = false;
        printSettings();
      }
      switch (settingselected)
      {
      case 0 :
        {
          if (TurnDetected2)  
          {
            if (rotationdirection2 and playmode > 0) 
            {
              playmode -= 1;
            } // decrase Position by 1
            if (rotationdirection2 == false and playmode < 4 ) 
            {
              playmode += 1;
            } // increase Position by 1
            TurnDetected2 = false;
            lcd.setCursor(0, 1);
            lcd.print("                ");
            lcd.setCursor(0, 1);
            if(playmode==0)
            {
              updown=0;
              lcd.print("Normal");
            }
            if(playmode==1)
            {
              updown=1;
              lcd.print("Reverse");
            }
            if(playmode==2)
            {

              lcd.print("Up & Down");
            }
            if(playmode==3)
            {

              randstep=0;
              lcd.print("Random"); 
            }
            if(playmode==4)
            {
              randstep=0;
              for(int i=0;i<16;i++)
              {
                randok[i]=true;
              }
              lcd.print("Random No Repeat");
            }
          }
          break;
        }
      case 1 :
        {
          if (TurnDetected2)  
          {
            if (rotationdirection2 and CCX > 0) 
            {
              CCX -= 1;
            } // decrase Position by 1
            if (rotationdirection2 == false and CCX < 127 ) 
            {
              CCX += 1;
            } // increase Position by 1
            TurnDetected2 = false;
            lcd.setCursor(0, 1);
            lcd.print("       ");
            lcd.setCursor(0, 1);
            lcd.print(CCX);
          }
          break;
        }
      case 2:
        {
          if (TurnDetected2)  
          {
            if (rotationdirection2 and CCY > 0) {
              CCY -= 1;
            } // decrase Position by 1
            if (rotationdirection2 == false and CCY < 127 ) 
            {
              CCY += 1;
            } // increase Position by 1
            TurnDetected2 = false;
            lcd.setCursor(0, 1);
            lcd.print("       ");
            lcd.setCursor(0, 1);
            lcd.print(CCY);
          }
          break;
        }
      case 3 :
        {
          for (int i = 0; i < 16; i++)
          {
            if (digitalRead(pinpush[i]) == LOW and (millis() - uptimepush > 250))  //push + debouncing, select a note
            {
              uptimepush = millis();
              lcd.setCursor(0, 1);
              lcd.print("                ");
              lcd.setCursor(0, 1);
              MidiNoteOff(midichannel, note[noteplayed][param1]);
              midichannel = i + 1;
              lcd.print(midichannel);
            }
          }
          break;
        }
      case 4 :
        {
          for (int i = 0; i < 16; i++)
          {
            if (digitalRead(pinpush[i]) == LOW and (millis() - uptimepush > 250))  //push + debouncing, select a note
            {
              uptimepush = millis();
              lcd.setCursor(0, 1);
              lcd.print("                ");
              lcd.setCursor(0, 1);
              midiInChannel = i + 1;
              lcd.print(midiInChannel);
            }
          }
          break;
        }
      case 5 :
        {
          if (TurnDetected2) 
          {
            if (rotationdirection2  and keyboard == false) {
              keyboard = true;
              lcd.setCursor(0, 1);
              lcd.print("             ");
              lcd.setCursor(0, 1);
              lcd.print("Yes");
              MidiNoteOff(midichannel, note[noteplayed][0]);
              for (int i = 0; i < 16; i++)
                note[i][0] = note[i][0] - keyreference + noteoffset;
            } // decrase Position by 1
            else if (rotationdirection2 == false and keyboard == true) 
            {
              keyboard = false;
              lcd.setCursor(0, 1);
              lcd.print("             ");
              lcd.setCursor(0, 1);
              MidiNoteOff(midichannel, note[noteplayed][0]);
              for (int i = 0; i < 16; i++)
                note[i][0] = note[i][0] - noteoffset + keyreference;
              lcd.print("No");
            } // increase Position by 1
            TurnDetected2 = false;
          }
          break;
        }
      case 6 :
        {
          if (TurnDetected2) {
            if (rotationdirection2 and keyreference > 0) {
              keyreference -= 1;
            } // decrase Position by 1
            else if (rotationdirection2 == false and keyreference < 127 ) {
              keyreference += 1;
            } // increase Position by 1
            TurnDetected2 = false;
            lcd.setCursor(0, 1);
            lcd.print("             ");
            lcd.setCursor(0, 1);
            lcd.print(humannotes[keyreference]);
            if (keyboard == true and keyreference != previouskeyreference)
            {
              MidiNoteOff(midichannel, note[noteplayed][0]);
              for (int i = 0; i < 16; i++)
                note[i][0] = note[i][0] + previouskeyreference - keyreference;
            }
            previouskeyreference = keyreference;
          }
          break;
        }
      case 7 :
        {
          if (TurnDetected2)  {
            if (rotationdirection2 and clock > 0) {
              clock -= 1;
            } // decrase Position by 1
            if (rotationdirection2 == false and clock < 2 ) {
              clock += 1;
            } // increase Position by 1
            if (clock == 2 and play == true)
              SerialSelected->write(0xfa);
            TurnDetected2 = false;
            lcd.setCursor(0, 1);
            lcd.print("             ");
            lcd.setCursor(0, 1);
            if (clock == 0)
              lcd.print("Internal");
            else if (clock == 1)
              lcd.print("Midi In");
            else if (clock == 2)
              lcd.print("master");
          }
          break;
        }
      case 8 :
        {
          if (digitalRead(pinpush[0]) == LOW and (millis() - uptimepush > 250))  //push + debouncing, select a note
          {
            uptimepush = millis();
            timequant();
            lcd.setCursor(0, 1);
            lcd.print("                ");
            lcd.setCursor(0, 1);
            lcd.print("Done :)");
          }
          break;
        }
      case 9 :
        {
          if (digitalRead(pinpush[0]) == LOW and (millis() - uptimepush > 250))  //push + debouncing, select a note
          {
            uptimepush = millis();
            MidiNoteOff(midichannel, note[noteplayed][0]);
            tempo = 80;
            timetempo = (double)((1) / (tempo)) * 60000;
            OCR5A = consttimer5 / tempo;
            for (int i = 0; i < 16; i++)
            {
              note[i][0] = 48; //note C4
              note[i][1] = 45; //vel
              note[i][2] = 12;  //time div 1
              note[i][3] = 500; //continuous note duration, 1s
              note[i][4] = 1 * timetempo;
            }
            param2 = 2;
            lastnote = 15;
            lcd.setCursor(0, 1);
            lcd.print("                ");
            lcd.setCursor(0, 1);
            lcd.print("Done :)");
            noteoffset = 0;
            previousnoteoffset = 0;
          }
          break;
        }
      case 10 :
        {
          if (digitalRead(pinpush[0]) == LOW and (millis() - uptimepush > 250))  //push + debouncing, select a note
          {
            uptimepush = millis();
            saveSettings();
            lcd.setCursor(0, 1);
            lcd.print("                ");
            lcd.setCursor(0, 1);
            lcd.print("Done :)");
          }
          break;
        }
      case 11 :
        {
          break;
        }
      }
    }
    while (exitsetting == false);
    TurnDetected1 = false;
    TurnDetected2 = false;
    TurnDetected3 = false;
    lcd.clear();
    uptimepushsetting = millis();
  }
}

void playpause()
{
  if ((digitalRead(pinplay) == LOW) and ((millis() - uptimeplay) > 250) and isplaypush == false )
  {
    isplaypush = true;
    uptimeplay = millis();
  }
  else if (digitalRead(pinplay) == HIGH and isplaypush == true)
  {
    isplaypush = false;
    play = !play;
    noteplayedtime = millis();
    if (clock == 2)
    {
      if (play == true)
        SerialSelected->write(0xfa);//start
      if (play == false)
        SerialSelected->write(0xfc);//stop
    }//SongSetting();
  }
  else if ((digitalRead(pinplay) == LOW) and ((millis() - uptimeplay) > 500) and isplaypush == true )
  {
    uptimeplay = millis();
    if (song == false)
      SongSetting();
    else
      QuitSong();
    uptimeplay = millis();
  }
}

void playstop()
{
  if (((digitalRead(pinstop) == LOW) and ((millis() - uptimestop) > 250) ) or data == 0xfc)
  {
    play = false;
    uptimestop = millis();
    MidiNoteOff(midichannel, note[noteplayed][0]);
    digitalWrite(redled[noteplayed], LOW);
    noteplayed = lastnote;
    if (clock == 2)
      SerialSelected->write(0xfc);  //stop
  }
}

void Display()
{
  if ((millis() - timedisplaytempo) > 1000)
  {
    if (lcdchange)
    {
      lcdchange = false;
      lcd.clear();
    }
    lcd.setCursor(0, 0);
    lcd.print("Step");
    lcd.setCursor(6, 0);
    lcd.print(noteselect + 1);
    lcd.setCursor(0, 1);
    if (param == 0)
    {

      lcd.print("Note=");
      if (keyboard == false) {
        if (note[noteselect][0] >= 0 and note[noteselect][0] <= 127)
          lcd.print(humannotes[(int)note[noteselect][0]]);
        else if (note[noteselect][0] < 0)
          lcd.print(humannotes[0]);
        else if (note[noteselect][0] > 127)
          lcd.print(humannotes[127]);
        lcd.print("      ");
      }
      else if (keyboard == true)
      {
        lcd.print("+");
        if (note[noteselect][0] >= 0 and note[noteselect][0] <= 127)
          lcd.print(note[noteselect][0]);
        else if (note[noteselect][0] < 0)
          lcd.print("0");
        else if (note[noteselect][0] > 127)
          lcd.print("127");
        lcd.print("      ");
      }
    }
    else if (param == 1)
    {
      lcd.print("Vel=");
      lcd.print((int)note[noteselect][1]);
    }
    else if (param == 2)
    {
      lcd.print("Time Div=");
      lcd.print(timediv[(int)note[noteselect][2]]);
    }
    else if (param == 3)
    {
      lcd.print("Dur=");
      lcd.print(note[noteselect][3]);
      lcd.print("ms");
    }
  }
}

void Sync()
{
  unsigned long int sum = 0;
  for (int i = 0; i < 5; i++)
    sum += timetab[i];
  sum /= 5;
  timetempo = (sum * 24.) / 1000.;
  tempo = (int)(((1.) / (timetempo)) * 60000);
  OCR5A = consttimer5 / tempo;
  if (previoustempo != tempo) {
    previoustempo = tempo;
    for (int i = 0; i < 16; i++)
    {
      note[i][4] = timetempo * timedivms[(int)note[i][2]];
    }
  }
}

void keyboardTracking()
{
  noteoffset = (int)notebyte;
  MidiNoteOff(midichannel, note[noteplayed][0]);
  digitalWrite(redled[noteplayed], LOW);
  for (int l = 0; l < 16; l++)
    note[l][0] = note[l][0] - previousnoteoffset + noteoffset;
  previousnoteoffset = noteoffset;
  if(updown==0)
    noteplayed = 15;
  else if(updown==1)
    noteplayed = 0;
  noteplayedtime = 100000000;
}

void startrecord()
{
  if (digitalRead(pushrecord) == LOW and millis() - uptimepushrecord > 250)
  {
    song=false;
    uptimepushrecord = millis();
    play = false;
    MidiNoteOff(midichannel, note[noteplayed][0]);
    TIMSK3 |= (0 << OCIE3A);  // disable timer compare interrupt //we stop midi out (but not master clock!)
    OCR4A = 1;  // compare match register //we speed up midi in
    TCCR4B |= (0 << CS40);
    MidiNoteOff(midichannel, note[noteplayed][0]);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("On Air");
    lcd.setCursor(0, 1);
    lcd.print("note number ");
    lcd.print(noteended + 1);
    record = true;
    int temp = 0;
    for (int i = 0; i < 16; i++)
      state[i] = 0;
    do {
      if (temp != noteended)
      {
        lcd.setCursor(11, 1);
        lcd.print("     ");
        lcd.setCursor(12, 1);
        lcd.print(noteended + 1);
        temp = noteended;
      }
      if (digitalRead(pushrecord) == LOW and millis() - uptimepushrecord > 250)
      {
        uptimepushrecord = millis();
        play = true;
        lcd.clear();
        TIMSK3 |= (1 << OCIE3A);  // enable timer compare interrupt
        OCR4A = 13;  // compare match register
        TCCR4B |= (1 << CS40);
        return;
      }
      for (int i = 0; i < 16; i++)
      {
        if (digitalRead(pinpush[i]) == LOW and millis() - uptimepush > 250)
        {
          uptimepush = millis();
          lastnote = i;
        }
      }
    }
    while (record == true);
    TIMSK3 |= (1 << OCIE3A);  // enable timer compare interrupt
    OCR4A = 13;  // compare match register
    TCCR4B |= (1 << CS40);
    lcd.clear();
  }
}

void quantization()
{
  for (int i = 0; i < lastnote + 1; i++) //each note played
  {
    for (int j = 0; j < 24; j++) //each timediv intervals
    {
      if (j == 0 && note[i][3] < timetempo * timedivms[0])
      {
        note[i][2] = 0;
        break;
      }
      if (j == 1 && note[i][3] > timetempo * timedivms[24])
      {
        note[i][2] = 24;
        break;
      }
      if (note[i][3] > timetempo * timedivms[j] && note[i][3] < timetempo * timedivms[j + 1])//the time is between these two timediv
      {
        if (abs(timetempo * timedivms[j] - note[i][3]) < abs(timetempo * timedivms[j + 1] - note[i][3]))
        {
          note[i][2] = j;
        }
        else
        { 
          note[i][2] = j + 1;
        }
        break;
      }
    }
    note[i][4] = timetempo * timedivms[(int)note[i][2]];
  }
}

void timequant()
{
  for(int i=0;i<16;i++)
    note[i][3]=note[i][4];
}

void keyboardrecordnoteon() {     //call this function in the midi in isr with keyboardtracking, if record==true;

  if (noterecorded <= lastnote)
  {
    state[noterecorded] = 1; //note pressed
    keynote[noterecorded][0] = notebyte; //the pitch if the note is pressed
    keynote[noterecorded][1] = velocitybyte; //its velocity
    temptime[noterecorded] = micros();  //when it is pressed
    //if(noterecorded!=lastnote)
    noterecorded++;  //we shift to the next one
  }
}

void  keyboardrecordnoteoff()
{
  for (int i = 0; i <= noterecorded; i++)
  {
    if (notebyte == keynote[i][0] and state[i] == 1)
    {
      state[i] = 2; //note released
      keynote[noteended][2] = (micros() - temptime[noteended]) / 1000;
      noteended++;
    }
  }
  if (noteended >= lastnote + 1)
  {
    record = false;
    noterecorded = 0;
    noteended = 0;
    for (int i = 0; i <= lastnote; i++)
    {
      note[i][0] = keynote[i][0]; //pitch
      note[i][1] = keynote[i][1]; //velocity
      note[i][3] = keynote[i][2]; //time (continuous)
      state[i] = 0;
    }
    quantization();
    param2 = 3;
    if(param==2)
      param=3;
    play = true;
  }
}

void SongSetting()
{
  uptimeplay = millis();
  isplaypush = true;
  bool playrelease = false;
  play = false;
  song = true;
  MidiNoteOff(midichannel, note[noteplayed][0]);
  digitalWrite(redled[noteplayed], LOW);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Song Mode");
  int i = 0;
  lcd.setCursor(0, 1);
  lcd.print("Sequence ");
  lcd.print(i + 1);
  bool ok = false;
  bool seqdisp[16];
  for (int i = 0; i < 16; i++)
  {
    if (readInt(168 * i) == 6666)
    {
      seqdisp[i] = true;
      digitalWrite(redled[i], HIGH);
    }
    else
      seqdisp[i] = false;
  }

  do {
    for (int j = 0; j < 16; j++)
    {
      if (digitalRead(pinpush[j]) == LOW and (millis() - uptimepush > 250) and  seqdisp[j] == true) //push + debouncing, select a note
      {
        lcd.clear();
        uptimepush = millis();
        lcd.setCursor(0, 0);
        lcd.print("Song Mode");
        seqseq[i] = j;
        i++;
        lcd.setCursor(0, 1);
        lcd.print("Sequence ");
        lcd.print(i + 1);
      }
    }
    if (digitalRead(pinplay) == HIGH and playrelease == false)
    {
      playrelease = true;
      isplaypush = false;
    }
    if (digitalRead(pinplay) == LOW and playrelease == true)
      isplaypush = true;
    if ((digitalRead(pinplay) == HIGH) and  isplaypush == true and playrelease == true)
    {
      uptimeplay = millis();
      isplaypush = false;
      ok = true;
      seqseq[i] = -1;
    }
  } 
  while (ok == false and i != 1027);
  if (i == 0)
    song == false;
  seqplayed = -1;

  for (int j = 0; j < 16; j++)
  {
    if (seqdisp[j] == true)
      digitalWrite(redled[j], LOW);
  }
  if(playmode==0)
    noteplayed = lastnote;
  if(playmode==1)
    noteplayed=0;
  if(playmode==2)
  {
    updown=1;
    noteplayed=1; 
  }
  if(playmode==3)
    randstep=lastnote;
  if(playmode==4)
    randstep=lastnote;  
  lcd.clear();
  play = true;
}

void QuitSong()
{
  song = false;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Song Ended");
  do {
    ;
  } 
  while (digitalRead(pinplay) == LOW);
  lcd.clear();
  play=true;
}

void lightshow()
{
  for (int i = 0; i < 16; i++)
    digitalWrite(redled[i], LOW);
  int j=0;
  unsigned long int temptime=millis();          
  do{
    digitalWrite(redled[j],HIGH);
    if(millis()-temptime>25)
    {
      temptime=millis();
      if(j>3)
        digitalWrite(redled[j-4],LOW);
      j++;
      if(j<16)
        digitalWrite(redled[j],HIGH);
    }
  }
  while(j<19); 
}
void setup() {

  pinMode(midiswitch,INPUT);
  int temp=0;
  testmidiswitch=false;
  for(int i=0;i<10000;i++)
  {
    if(digitalRead(midiswitch)!=0)
      temp++;
  }
  if(temp<1000)
  {
    SerialSelected=&Serial1;
    testmidiswitch=true; 
  }
  else
    SerialSelected=&Serial;
  SerialSelected->begin(31250);     // midi  working frequency
  // Serial.begin(9600);      // DEBUG mode
  pinMode(PinCLK1, INPUT);
  pinMode(PinDT1, INPUT);
  pinMode(PinSW1, INPUT);
  digitalWrite(PinSW1, HIGH); // Pull-Up resistor for switch
  pinMode(PinCLK2, INPUT);
  pinMode(PinDT2, INPUT);
  pinMode(PinSW2, INPUT);
  digitalWrite(PinSW2, HIGH); // Pull-Up resistor for switch
  pinMode(PinCLK3, INPUT);
  pinMode(PinDT3, INPUT);
  pinMode(PinSW3, INPUT);
  digitalWrite(PinSW3, HIGH); // Pull-Up resistor for switch
  attachInterrupt (0, isr1, FALLING); // interrupt 0 connected to pin 2 on Arduino 2560
  attachInterrupt (2, isr2, FALLING); // interrupt 5 connected to pin 21 on 2560
  attachInterrupt(3, isr3, FALLING); //interrupt 3 connected to pin 20 on 2560
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.print("MidiCon-X16-ISR");
  delay(1000);
  lcd.clear();
  // Print a message to the LCD.
  for (int i = 0; i < 16; i++)
  {
    pinMode(pinpush[i], INPUT_PULLUP);
    pinMode(redled[i], OUTPUT);
    note[i][0] = 48; //note C4
    note[i][1] = 45; //vel
    note[i][2] = 12;  //time div 1
    note[i][3] = 500; //continuous note duration, 500ms
  }
  pinMode(pushmem, INPUT_PULLUP);
  pinMode(pushsetting, INPUT_PULLUP);
  pinMode(pinplay, INPUT_PULLUP);
  pinMode(pinstop, INPUT_PULLUP);
  pinMode(pushrecord, INPUT_PULLUP);
  loadSettings();
  if(playmode==1)
    updown=1;
  randomSeed(analogRead(15));
  lcd.clear();
  tempo = 80;
  timetempo = (double)((1) / (tempo)) * 60000;
  OCR5A = consttimer5 / tempo;
  for (int i = 0; i < 16; i++)
  {
    note[i][4] = 1 * timetempo; //continuous note duration, ms
  }
  timetempo = (double)((1) / (tempo)) * 60000;
  //we use timer 3 (16bit) to manage note playing interrupt
  noInterrupts();           // disable all interrupts
  TCCR3A = 0;  //we set entire TCCR3A register to 0
  TCCR3B = 0; //same fore TCCR3B
  TCNT3 = 0; // set the counter to 0
  OCR3A = 300;            // compare match register
  TCCR3B |= (1 << WGM32);   // CTC mode
  TCCR3B |= (1 << CS30);    // no prescaler
  TIMSK3 |= (1 << OCIE3A);  // enable timer compare interrupt
  //speed 18.5us
  //min theoretical duration of a midi note->0.5ms, so it's OKKKKKK, but I keep min duration to 1ms, synth can't even play it, so no interest to go down again...
  TCCR4A = 0;  //we set entire TCCR3A register to 0
  TCCR4B = 0; //same fore TCCR3B
  TCNT4 = 0; // set the counter to 0
  OCR4A = 13;  // compare match register
  TCCR4B |= (1 << WGM42);   // CTC mode
  TCCR4B |= (1 << CS41);    // n64 prescaler, OCRA ->13 -> 19230Hz
  TCCR4B |= (1 << CS40);
  TIMSK4 |= (1 << OCIE4A);  // enable timer compare interrupt
  TCCR5A = 0;  //we set entire TCCR3A register to 0
  TCCR5B = 0; //same fore TCCR3B
  TCNT5 = 0; // set the counter to 0
  OCR5A = 7812;  // compare match register
  TCCR5B |= (1 << WGM52);   // CTC mode
  TCCR5B |= (1 << CS51);    // n64 prescaler,
  TCCR5B  |= (1 << CS50);
  TIMSK5 |= (1 << OCIE5A);  // enable timer compare interrupt
  //need to tune timer 5 but as the sequencer is not soldered yet, we must wait to do some testss
  interrupts();             // enable all interrupt

}

void loop() {
  BlueLedBlink();  //tempo led
  Display();  //displays all we need
  for ( int k = 0; k < 16; k++)
  {
    HardestButtonToButton(k);  //what to do if a button is push
  }
  if (!(digitalRead(PinSW1)) and millis() - uptimeencoder1 >= 500)
  {
    PushEncoder1();  //what to do if we push the 1st encoder
  }
  if (TurnDetected1)
  {
    TurnEncoder1();//what to do if we turn the 1st encoder
  }
  if (!(digitalRead(PinSW2)) and millis() - uptimeencoder2 >= 500)
  {
    PushEncoder2(); //what to do if we push the 2nd encoder
  }
  if (TurnDetected2)
  {
    TurnEncoder2(); //what to do if we turn the 2nd encoder
  }
  Settings();
  playpause();
  playstop();
  setTempo();
  startrecord();
  Memory_All_alone_in_the_moonlight_I_can_smile_at_the_old_days_I_was_beautiful_then_I_remember_The_time_I_knew_what_happiness_was_Let_the_memory_live_again();
}






